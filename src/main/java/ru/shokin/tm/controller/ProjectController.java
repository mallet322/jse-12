package ru.shokin.tm.controller;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.service.ProjectService;
import ru.shokin.tm.service.ProjectTaskService;

import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    public int createProject(final Long userId) {
        System.out.println("Create project");
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        if (projectService.create(userId, name, description) == null) {
            System.out.println("failed");
            System.out.println("Sorry, we cannot create project with null argument.");
            System.out.println("Please, create project again :)");
            return -1;
        }
        System.out.println("ok");
        return 0;
    }

    public int clearProject() {
        System.out.println("Clear project");
        projectService.clear();
        System.out.println("ok");
        return 0;
    }

    public int listProject(final Long userId) {
        System.out.println("Projects list");
        viewTasks(projectService.findAllByUserId(userId));
        System.out.println("ok");
        return 0;
    }

    public int viewProjectById(final Long userId) {
        System.out.println("Please, enter project id:");
        final long id = scanner.nextLong();
        final Project project = projectService.findByUserIdAndId(userId, id);
        viewProject(project);
        return 0;
    }

    public int viewProjectByName(final Long userId) {
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        final Project project = projectService.findByUserIdAndName(userId, name);
        viewProject(project);
        return 0;
    }

    public int updateProjectById(final Long userId) {
        System.out.println("Update project by id");
        System.out.println("Please, enter project id:");
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = projectService.findByUserIdAndId(userId, id);
        if (project == null) {
            System.out.println("failed");
            return 0;
        }
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        projectService.update(id, name, description);
        System.out.println("ok");
        return 0;
    }

    public int updateProjectByName(final Long userId) {
        System.out.println("Update project by name");
        System.out.println("Please, enter project name:");
        final String projectName = scanner.nextLine();
        final Project project = projectService.findByUserIdAndName(userId, projectName);
        if (project == null) {
            System.out.println("failed");
            return 0;
        }
        System.out.println("Please, enter new project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter new project description:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("ok");
        return 0;
    }

    public int removeProjectById(final Long userId) {
        System.out.println("Remove project by id");
        System.out.println("Please, enter project id:");
        final long id = scanner.nextLong();
        final Project project = projectService.removeById(userId, id);
        if (project == null) {
            System.out.println("failed");
        } else {
            projectTaskService.removeProjectWithTasks(userId, id);
            System.out.println("ok");
        }
        return 0;
    }

    public int removeProjectByName(final Long userId) {
        System.out.println("Remove project by name");
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(userId, name);
        if (project == null) {
            System.out.println("failed");
        } else {
            final long projectId = project.getId();
            projectTaskService.removeProjectWithTasks(userId, projectId);
            System.out.println("ok");
        }
        return 0;
    }

    private void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("View project");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("***");
        System.out.println("User ID: " + project.getUserId());
        System.out.println("ok");
    }

    public void viewTasks(List<Project> projects) {
        if (projects == null || projects.isEmpty()) {
            System.out.println("Sorry, there are no tasks in the program...");
            return;
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
    }

}