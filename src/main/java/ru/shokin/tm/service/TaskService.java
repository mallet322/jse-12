package ru.shokin.tm.service;

import ru.shokin.tm.entity.Task;
import ru.shokin.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final Long userId, final String name, final String description) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(userId, name, description);
    }

    public Task update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task removeById(final Long userId, final Long id) {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepository.removeById(userId, id);
    }

    public Task removeByName(final Long userId, final String name) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(userId, name);
    }

    public Task findById(final Long id) {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findByUserIdAndId(final Long userId, final Long id) {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepository.findByUserIdAndId(userId, id);
    }

    public Task findByUserIdAndName(final Long userId, final String name) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByUserIdAndName(userId, name);
    }

    public List<Task> findAllByUserId(final Long userId) {
        if (userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (projectId == null || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

}