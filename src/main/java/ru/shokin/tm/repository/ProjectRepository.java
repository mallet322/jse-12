package ru.shokin.tm.repository;

import ru.shokin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public Project create(final Long userId, final String name, final String description) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public Project removeById(final Long userId, final Long id) {
        final Project project = findByUserIdAndId(userId, id);
        if (project == null) return null;
        projects.remove(project);

        return project;
    }

    public Project removeByName(final Long userId, final String name) {
        final Project project = findByUserIdAndName(userId, name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project findById(final Long id) {
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project findByName(final String name) {
        for (final Project project : projects) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    public List<Project> findAll() {
        return projects;
    }

    public Project findByUserIdAndId(final Long userId, final Long id) {
        for (final Project project : projects) {
            final Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project findByUserIdAndName(final Long userId, final String name) {
        for (final Project project : projects) {
            final Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : findAll()) {
            final Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(project);
        }
        return result;
    }

}