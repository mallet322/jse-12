package ru.shokin.tm.entity;

public class Project {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    private Long userId;

    public Project() {
    }

    public Project(final String name) {
        this.name = name;
    }

    public Project(final Long userId, final String name, final String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Project id: " + id + ", Project name: " + name + ", Project description: " + description;
    }

}