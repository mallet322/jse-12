package ru.shokin.tm.entity;

import ru.shokin.tm.enumerated.Role;
import ru.shokin.tm.util.HashUtil;

public class User {

    private Long id = System.nanoTime();

    private String login;

    private String password;

    private Role role;

    private String firstName = "";

    private String lastName = "";


    public User() {
    }

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = HashUtil.encryptMD5(password);
        this.role = Role.USER;
    }

    public User(String login, String firstName, String lastName) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = HashUtil.encryptMD5(password);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User id: " + id + ", Username: " + login + ", User password: " + password;
    }

}