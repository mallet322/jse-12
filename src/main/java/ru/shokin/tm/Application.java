package ru.shokin.tm;

import static ru.shokin.tm.constant.TerminalConst.*;

import ru.shokin.tm.entity.User;
import ru.shokin.tm.enumerated.Role;
import ru.shokin.tm.repository.ProjectRepository;
import ru.shokin.tm.repository.TaskRepository;
import ru.shokin.tm.repository.UserRepository;
import ru.shokin.tm.controller.SystemController;
import ru.shokin.tm.controller.ProjectController;
import ru.shokin.tm.controller.TaskController;
import ru.shokin.tm.controller.UserController;
import ru.shokin.tm.service.*;

import java.util.Scanner;

public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final SystemController systemController = new SystemController();

    private final ProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final UserController userController = new UserController(userService);

    {
        final User me = userRepository.create("mallet", "qwerty123", Role.ADMIN);
        final User admin = userRepository.create("ADMIN", "qwerty123", Role.ADMIN);
        final User user = userRepository.create("TEST", "qwerty123", Role.USER);

        projectService.create(me.getId(), "MY PROJECT", "PROJECT DESC");
        projectService.create(admin.getId(), "DEMO PROJECT 1", "PROJECT DESC 1");
        projectService.create(user.getId(), "DEMO PROJECT 2", "PROJECT DESC 2");
        taskService.create(me.getId(), "MY TASK", "PROJECT TASK");
        taskService.create(admin.getId(), "DEMO TASK 1", "TASK DESC 1");
        taskService.create(user.getId(), "DEMO PROJECT 2", "TASK DESC 2");
    }

    public static void main(final String[] args) {
        final Application app = new Application();
        app.run(args);
        app.systemController.displayWelcome();
        app.authentication();
        app.process();
    }

    private void authentication() {
        final int login = userController.authenticationUser();
        if (login == -1) {
            System.exit(-1);
        }
    }

    private void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command, userService.currentUser.getId());
            System.out.println();
        }
    }

    private void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        final int result = run(param, userService.currentUser.getId());
        System.exit(result);
    }

    private int run(final String command, final Long userId) {
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.displayExit();

            case PROJECT_CREATE:
                return projectController.createProject(userId);
            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_LIST:
                return projectController.listProject(userId);
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById(userId);
            case PROJECT_VIEW_BY_NAME:
                return projectController.viewProjectByName(userId);
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName(userId);
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById(userId);
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById(userId);
            case PROJECT_UPDATE_BY_NAME:
                return projectController.updateProjectByName(userId);

            case TASK_CREATE:
                return taskController.createTask(userId);
            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_LIST:
                return taskController.listTask(userId);
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById(userId);
            case TASK_VIEW_BY_NAME:
                return taskController.viewTaskByName(userId);
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName(userId);
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById(userId);
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById(userId);
            case TASK_UPDATE_BY_NAME:
                return taskController.updateTaskByName(userId);
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskController.removeTaskFromProjectByIds();
            case TASK_LIST_BY_PROJECT_ID:
                return taskController.listTaskByProjectId();

            case USER_CREATE:
                return userController.createUser();
            case USER_CLEAR:
                return userController.clearUser();
            case USER_LIST:
                return userController.listUser();
            case USER_ADD_INFORMATION:
                return userController.addUserInformation();
            case USER_VIEW_INFORMATION:
                return userController.viewCurrentUser();
            case USER_UPDATE_INFORMATION:
                return userController.updateCurrentUser();
            case USER_CHANGE_PASSWORD:
                return userController.changePassword();
            case USER_VIEW_BY_LOGIN:
                return userController.viewUserByLogin();
            case USER_REMOVE_BY_LOGIN:
                return userController.removeUserByLogin();
            case USER_UPDATE_BY_LOGIN:
                return userController.updateUserByLogin();

            case LOGIN:
                return userController.authenticationUser();
            case LOGOFF:
                return userController.endingSession();

            default:
                return systemController.displayError();
        }
    }

}